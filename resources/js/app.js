require('./bootstrap');

window.Vue = require('vue');
import VueRouter from 'vue-router'
import Notifications from 'vue-notification'
import VueProgressiveImage from 'vue-progressive-image'
import moment from 'moment'

import App from './views/App'
import store  from './store/store.js';
import * as types from './store/types.js'

import { routes } from './routes'


Vue.use(VueRouter)
Vue.use(Notifications)
Vue.use(VueProgressiveImage)

Vue.filter ('currency', (value) => {
   return '₦' + value.toLocaleString() + '.00';
});

Vue.filter('formatDate', function(value) {
  if (value) {
    return moment(String(value)).format('MMM Do, YYYY')
  }
});



Vue.component('example-component', require('./components/ExampleComponent.vue').default);


const router = new VueRouter({
  mode: 'history',
  routes
});

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (store.getters.token == null) {
            next({
                path: '/login',
                params: { nextUrl: to.fullPath }
            })
        } else {
            const user = store.getters.user;
            if (to.matched.some(record => record.meta.is_admin)) {
                if (user.is_admin == 1) {
                    next()
                }
                else {
                    next({ name: 'userboard' })
                }
            }
            else if (to.matched.some(record => record.meta.is_user)) {
                if (user.is_admin == 0) {
                    next()
                }
                else {
                    next({ name: 'admin' })
                }
            }
            next()
        }
    } else {
        next()
    }
});

const app = new Vue({
    el: '#app',
    components: { App },
    router,
    store,
    
});
