/** Token Key Values */
export const TOKEN_NAME_JWT = 'InventoryApp';
export const TOKEN_NAME_USER = 'InventoryApp.user';
export const TOKEN_NAME_CART = 'InventoryApp.cart';
export const TOKEN_NAME_CARTCOUNT = 'InventoryApp.cartCount';

/** Mutations */
export const ADD_TO_CART = 'ADD_TO_CART';
export const REMOVE_FROM_CART = 'REMOVE_FROM_CART';

/** CONSTANT VARIABLES */
export const PAYSTACK_KEY = 'pk_test_6e7cb84753638f824007f4fe6e56b58ab03becf4';