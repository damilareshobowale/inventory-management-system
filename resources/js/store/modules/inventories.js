import * as types from '../types';
import toast from '../../services/toast';

const state = {
    isLoading:  false,
    inventories: [],
    inventory: [],
    isEmpty: false,
    isSuccess: false,
};

const mutations = {
    'GET_ALL_INVENTORIES' (state, data) {
        state.inventories = data;
        state.isLoading = false;
    },
    'SET_IS_EMPTY_FALSE' (state, data) {
        state.isEmpty = true;
    },
    'ADD_TO_INVENTORY' (state, data) {
        state.isSuccess = true;
    },
    'SET_SUCCESS_STATE' (state, data) {
        state.isSuccess = data;
    },
    'GET_SINGLE_INVENTORY' (state, data) {
        state.inventory = data;
    },
}

const actions = {
    getAllInventories({commit}) {
        axios.get("/api/inventories/").then(response => {
            const inventories = response.data;
            commit('GET_ALL_INVENTORIES', inventories);
        }).catch(error => {
            toast.error('Inventories cannot be loaded. Please check your network');
            commit('SET_IS_EMPTY_FALSE');
        });
    },
    addInventory({commit}, {name, quantity, price, description, image}) {
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + this.getters.token;

        axios.post('/api/inventories', {name, quantity, price, description, image}).then(response => {
            commit('ADD_TO_INVENTORY', true);
            toast.success('-:) Yay!, New item has been added to inventory.');
        }).catch(error => {
            toast.error('Guess there was an error saving your item');
        });
    },
    updateInventory({commit}, {name, quantity, price, description, image, id}) {
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + this.getters.token;

        axios.put('/api/inventories/'+ id, {name, quantity, price, description, image}).then(response => {
            console.log(response);
            commit('UPDATE_INVENTORY', true);
            toast.success('-:) Yay!, New item has been updated.');
        }).catch(error => {
            toast.error('Guess there was an error updating your item');
        });
    },
    deleteInventory({commit}, {id, index}) {
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + this.getters.token;

        axios.get('/api/inventories/'+id + '/delete').then(response => {
            commit('SET_SUCCESS_STATE', true);
            this.getters.inventories.splice(index, 1);
        }).catch(error => {
             commit('SET_SUCCESS_STATE', true);
        });
    },
    getSingleInventories({commit}, id) {
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + this.getters.token;

        axios.get('/api/inventories/'+ id).then(response => {
            const inventory = response.data;
            commit('GET_SINGLE_INVENTORY', inventory.data);
            commit('CHANGE_HEADER_TITLE', inventory.data.name);
        }).catch(error => {
            commit('SET_SUCCESS_STATE', false);
        });
    }
}

const getters = {
    inventories: state => {
        return state.inventories;
    },
    isLoading: state => {
        return state.isLoading;
    },
    isEmpty: state => {
        return state.isEmpty;
    },
    isSuccess: state => {
        return state.isSuccess;
    },
    token: state => {
        return state.token;
    },
    inventory: state => {
        return state.inventory;
    },
}

export default {
    state,
    mutations,
    actions,
    getters
}
