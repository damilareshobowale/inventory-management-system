import * as types from '../types';

const state = {
    title: 'Welcome to FINT eCommerce App',
    tokens: localStorage.getItem([types.TOKEN_NAME_JWT]) ? localStorage.getItem([types.TOKEN_NAME_JWT]) : null,
};

const mutations = { 
    'CHANGE_HEADER_TITLE' (state, title) {
        state.title = title;
    }
};

const actions = {
    changeHeaderTitle({commit}, title) {
        commit('CHANGE_HEADER_TITLE', title);
    }
};

const getters = {
    title: state => {
        return state.title;
    }
}

export default {
    state,
    mutations,
    actions,
    getters
}