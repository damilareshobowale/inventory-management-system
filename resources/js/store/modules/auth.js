import * as types from '../types';
import toast from '../../services/toast';

const user = localStorage.getItem(types.TOKEN_NAME_USER);
const token = localStorage.getItem(types.TOKEN_NAME_JWT);
const state = {
    token: token ? token : null,
    user: user ? JSON.parse(user) : '',
    isLoggedIn: token ? true : false,
    resetToken: '',
};

const mutations = { 
    'DELETE_SESSION' (state) {
        state.token = null;
        state.user = '';
        state.isLoggedIn = false;

        this.commit('removeSession');
                   
    },
    'SET_SESSION' (state, {user, token}) {
        state.user = user;
        state.token = token;
        state.isLoggedIn = true;

        this.commit('saveSession');
    },
    'SET_ERROR' (state) {
        state.isLoggedIn = false;
    },
    'SET_RESET_TOKEN' (state, token) {
        state.resetToken = token;
    },
    saveSession(state) {
        localStorage.setItem(types.TOKEN_NAME_USER, JSON.stringify(state.user));
        localStorage.setItem(types.TOKEN_NAME_JWT, state.token);
    },
    removeSession() {
        localStorage.removeItem(types.TOKEN_NAME_USER)
        localStorage.removeItem(types.TOKEN_NAME_JWT);
    } 
};

const actions = {
    deleteSession({commit}) {
        commit('DELETE_SESSION');
    },
    
    loginSession({commit}, {login, password, redirectTo}) {
        axios.post('api/login', {login, password}).then(response => {
            const data = {
                user: response.data.user,
                token: response.data.token,
            }
            commit('SET_SESSION', data);
        }).catch(error => {
            toast.error(error.response.data.error);
        });
    },

    registerSession({commit}, {firstname, lastname, name, email, password, c_password}) {
        axios.post('api/register', {firstname, lastname, name, email, password, c_password}).then(response => {
            let user = response.data.user
            const data = {
                user: JSON.stringify(response.data.user),
                token: response.data.token,
            }
            commit('SET_SESSION', data);
        }).catch(error => {
             commit('SET_ERROR');
             toast.error(error.response.data.error);
        });;
    },
    forgotPassword({commit}, {login}) {
        axios.post('api/password/create', {login}).then(response => {
            commit('SET_SUCCESS_STATE', true);
            commit('SET_RESET_TOKEN', response.data);
        }).catch(error => {
              commit('SET_SUCCESS_STATE', false);
              console.log(error.response);
              toast.error(error.response.data.error);
        });;
    },
    resetPassword({commit}, {email, password, c_password, token}) {
        axios.post('api/password/reset', {email, password, c_password, token}).then(response => {
            commit('SET_SUCCESS_STATE', true);
            commit('SET_RESET_TOKEN', null);
        }).catch(error => {
              commit('SET_SUCCESS_STATE', false);
              toast.error(error.response.data.error);
        });;
    },
    validateTokenPassword({commit}, token) {
        axios.get('api/password/find/' + token).then(response => {
            console.log(response);
            commit('SET_SUCCESS_STATE', true);
            commit('SET_RESET_TOKEN', token);
        }).catch(error => {
              commit('SET_SUCCESS_STATE', false);
              toast.error(error.response.data.error);
        });;
    },
};

const getters = {
     token: state => {
        return state.token;
    },
    user: state => {
        return state.user;
    },
    isLoggedIn: state => {
        return state.isLoggedIn;
    },
    getResetToken: state => {
        return state.resetToken;
    }
}

export default {
    state,
    mutations,
    actions,
    getters,
}