<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        $user->name = "Admin";
        $user->firstname = 'Admin';
        $user->lastname = 'Test';
        $user->email = "admin@inventory.com";
        $user->password = bcrypt('123456');
        $user->is_admin = true;
        $user->save();
    }
}
