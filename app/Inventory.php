<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inventory extends Model
{
    //
    use SoftDeletes;

    protected $fillable = [
        'name', 'price', 'quantity', 'description', 'user_id' 
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
