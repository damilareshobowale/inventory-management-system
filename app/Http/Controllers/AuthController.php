<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Auth\Events\PasswordReset;
use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;

class AuthController extends Controller
{
    /** get the lists of all orders made by the user */
    public function index()
    {
        return response()->json(User::with(['orders'])->get());
    }

    /** handles login */
    public function login(Request $request)
    {
        $login_type = filter_var($request->input('login'), FILTER_VALIDATE_EMAIL ) ? 'email' : 'name';
        $request->merge([$login_type => $request->input('login')]);
        if ($login_type == 'email') {
            $validator = Validator::make($request->all(), [
                'email' => 'required|string|email',
            ]);
        }
        else {
            $validator = Validator::make($request->all(), [
                'name' => 'required|string',
            ]);
        }
        
        $validator = Validator::make($request->all(), [
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 401);
        }

        $status = 401;
        $response = ['error' => 'Unauthorised'];
        try {
            if (Auth::attempt($request->only($login_type, 'password'))) {
                $status = 200;
                $response = [
                    'user' => Auth::user(),
                    'token' => Auth::user()->createToken(env('TOKEN_NAME'))->accessToken,
                ];
            }
            else {
                $status = 400;
                $response = ['error' => 'Email and password is invalid'];
            }
        }
        catch (\Illuminate\Database\QueryException $exception) {
            return response()->json(['error' => $exception], 400);
        }
        catch (PDOException $exception) {
            return response()->json(['error' => 'Sorry, there was an error saving your information.'], 400);
        }

        return response()->json($response, $status);
    }

    /** handles register */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:users|max:50',
            'email' => 'required|unique:users|email',
            'password' => 'required|min:6',
            'c_password' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 401);
        }

        $data = $request->only(['firstname', 'lastname', 'name', 'email', 'password']);
        $data['password'] = bcrypt($data['password']);

        try { 
            $user = User::create($data);
        }
        catch (\Illuminate\Database\QueryException $exception) {
            return response()->json(['error' => 'Email already exists'], 400);
        }
        catch (PDOException $exception) {
            return response()->json(['error' => 'Sorry, there was an error saving your information.'], 400);
        }

        return response()->json([
            'user' => $user,
            'token' => $user->createToken(env('TOKEN_NAME'))->accessToken,
        ]);
    }

    public function logout (Request $request) {

        $token = $request->user()->token();
        $token->revoke();
    
        $response = 'You have been succesfully logged out!';
        return response($response, 200);
    
    }
}
