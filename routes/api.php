<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['json.response']], function () { 
    Route::post('/register', 'AuthController@register');

    Route::post('/login', 'AuthController@login');

    Route::post('/logout', 'AuthController@logout');

    Route::group([    
        'namespace' => 'Auth',    
        'middleware' => 'api',    
        'prefix' => 'password'
    ], function () {    
        Route::post('create', 'ResetPasswordController@create');

        Route::get('find/{token}', 'ResetPasswordController@find');
        
        Route::post('reset', 'ResetPasswordController@reset');
    });

    Route::get('/inventories', 'InventoryController@index');

    Route::group(['middleware' => ['auth:api']], function() {
        
        Route::get('users', 'Admin\UserController@index');
        
        Route::post('inventories', 'InventoryController@store');
        
        Route::get('courses/export', 'InventoryController@export');

        Route::get('/inventories/{inventory}', 'InventoryController@show');

        Route::put('/inventories/{inventory}', 'InventoryController@update');

        Route::get('/inventories/{inventory}/delete', 'InventoryController@destroy');
    });

});
